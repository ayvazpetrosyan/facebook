<?php

$config = include __DIR__ . '/config.php';
if (empty($config['clientId']) || empty($config['clientSecret'])) {
    echo 'Config is wrong';
    exit;
}
define('PROJECT_CONFIG', $config);

require_once __DIR__ . '/classes/application.php';
require_once __DIR__ . '/classes/db.php';
require_once __DIR__ . '/classes/instagram.php';
require_once __DIR__ . '/classes/helper.php';
require_once __DIR__ . '/actions/abstract_action.php';
require_once __DIR__ . '/actions/index.php';
require_once __DIR__ . '/actions/auth.php';
require_once __DIR__ . '/actions/deauth.php';
require_once __DIR__ . '/actions/delete.php';
require_once __DIR__ . '/actions/download.php';
require_once __DIR__ . '/actions/validate.php';