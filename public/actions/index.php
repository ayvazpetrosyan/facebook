<?php

class Index extends AbstractAction
{

    public function run()
    {
        if (!isset($_REQUEST['redirect_url'])) {
            echo 'Redirect URL is missing';
            exit;
        }
        $_SESSION['redirect_url'] = urldecode($_REQUEST['redirect_url']);
        $redirectUrl = PROJECT_CONFIG['domain'] . 'auth';

        if (isset($_REQUEST['test']) && $_REQUEST['test'] == 'test') {
            // PROJECT_CONFIG['clientId'] == 303325330795100
            // $redirectUrl == https://instagram.c-962.maxcluster.net/auth
            header('Location: https://api.instagram.com/oauth/authorize?client_id=762304692347894&redirect_uri=' . $redirectUrl . '&scope=user_profile,user_media&response_type=code');
            //header('Location: https://www.facebook.com/v19.0/dialog/oauth?client_id=762304692347894&display=page&extras={"setup":{"channel":"IG_API_ONBOARDING"}}&redirect_uri=' . $redirectUrl . '&response_type=code&scope=instagram_basic,instagram_manage_comments,pages_show_list,pages_read_engagement');
            exit;
        }

        header('Location: https://api.instagram.com/oauth/authorize?client_id=' . PROJECT_CONFIG['clientId'] . '&redirect_uri=' . $redirectUrl . '&scope=user_profile,user_media&response_type=code');
        exit;
    }
}
