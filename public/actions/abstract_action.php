<?php

abstract class AbstractAction
{
    abstract public function run();
}