<?php

class Auth extends AbstractAction
{
    public function run()
    {
        $redirectUrl = $_SESSION['redirect_url'];
        if (empty($redirectUrl)) {
            echo 'Redirect URL is missing';
            exit;
        }
        $code = isset($_REQUEST['code']) ? $_REQUEST['code'] : null;
        if (empty($code)) {
            $this->redirect($redirectUrl, [
                'success' => 0,
                'message' => 'Code is missing'
            ]);
        }
        $result = App()->Instagram()->getAccessToken($code, $this->isTesting($redirectUrl));
        if (!$result['success']) {
            $this->redirect($redirectUrl, [
                'success' => 0,
                'message' => $result['message']
            ]);
        }
        $accessToken = $result['token'];
        $userId = $result['user_id'];
        $expires = $result['expires'];

        $result = App()->Instagram()->getUser($userId, $accessToken);

        if (!$result['success']) {
            $this->redirect($redirectUrl, [
                'success' => 0,
                'message' => $result['message']
            ]);
        }
        $username = $result['user']['username'];
        $account = App()->Db()->fetchRow('SELECT * FROM `accounts` WHERE `user_id`=?;', [
            ['value' => $userId, 'type' => \PDO::PARAM_STR],
        ]);
        if (empty($account)) {
            App()->Db()->query('INSERT INTO `accounts` (`user_id`,`username`,`access_token`,`expires`,`created`) VALUES (?,?,?,?,NOW());', [
                ['value' => $userId, 'type' => \PDO::PARAM_STR],
                ['value' => $username, 'type' => \PDO::PARAM_STR],
                ['value' => $accessToken, 'type' => \PDO::PARAM_STR],
                ['value' => date('Y-m-d H:i:s', $expires), 'type' => \PDO::PARAM_STR]
            ]);
        } else {
            App()->Db()->query('UPDATE `accounts` SET `access_token`=?, `expires`=? WHERE `id`=?;', [
                ['value' => $accessToken, 'type' => \PDO::PARAM_STR],
                ['value' => date('Y-m-d H:i:s', $expires), 'type' => \PDO::PARAM_STR],
                ['value' => (int)$account['id'], 'type' => \PDO::PARAM_INT]
            ]);
        }
        $_SESSION['redirect_url'] = null;
        $this->redirect($redirectUrl, [
            'success' => 1,
            'token' => $accessToken,
            'username' => $username,
            'user_id' => $userId,
            'expires' => $expires
        ]);
    }

    private function redirect($url, $params)
    {
        if (parse_url($url, PHP_URL_QUERY)) {
            $url = $url . '&' . http_build_query($params);
        } else {
            $url = $url . '?' . http_build_query($params);
        }
        header('Location: ' . $url);
        exit;
    }

    private function isTesting($redirectUrl)
    {
        $result = false;
        if (strpos($redirectUrl,'http://sw6.loc') !== false) {
            $result = true;
        }

        return $result;
    }
}
