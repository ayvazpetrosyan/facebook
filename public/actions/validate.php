<?php

class Validate extends AbstractAction
{
    public function run()
    {
        $token = isset($_POST['token']) ? $_POST['token'] : null;
        $username = isset($_POST['username']) ? $_POST['username'] : null;
        $userId = isset($_POST['userId']) ? $_POST['userId'] : null;
        if (empty($token) || empty($username) || empty($userId)) {
            echo json_encode(['success' => false]);
            exit;
        }
        $account = App()->Db()->fetchRow('SELECT * FROM `accounts` WHERE `user_id`=? AND `access_token`=? AND `username`=?;', [
            ['value' => $userId, 'type' => \PDO::PARAM_STR],
            ['value' => $token, 'type' => \PDO::PARAM_STR],
            ['value' => $username, 'type' => \PDO::PARAM_STR]
        ]);
        if (empty($account)) {
            echo json_encode(['success' => false]);
            exit;
        }
        echo json_encode(['success' => true]);
        exit;
    }
}