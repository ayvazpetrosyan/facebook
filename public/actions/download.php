<?php

class Download extends AbstractAction
{
    public function run()
    {
        if (!isset($_REQUEST['username'])) {
            echo 'Username is missing';
            exit;
        }
        $username = $_REQUEST['username'];
        $account = App()->Helper()->getAccountByUsername($username);
        if (empty($account)) {
            header('Location: ' . PROJECT_CONFIG['domain'] . '?username=' . $username);
            exit;
        }
        $result = App()->Instagram()->getMedia($account['user_id'], $account['access_token']);
        if (!$result['success']) {
            echo $result['message'];
            exit;
        }

        echo json_encode($result['media']);
        exit;
    }
}