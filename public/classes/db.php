<?php

class Db
{

    /**
     * @var PDO
     */
    private $connection;

    /**
     * @var array
     */
    private $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    ];

    /**
     * Db constructor.
     */
    public function __construct()
    {
        $this->connection = new PDO("mysql:host=" . PROJECT_CONFIG['db']['host'] . ";port=" . PROJECT_CONFIG['db']['port'] . ";dbname=" . PROJECT_CONFIG['db']['name'], PROJECT_CONFIG['db']['user'], PROJECT_CONFIG['db']['pass'], $this->options);
    }

    /**
     * @param string $sql
     * @param array $params
     * @return bool
     */
    public function query($sql, $params = [])
    {
        if ($stmt = $this->connection->prepare($sql)) {
            $stmt = $this->bindParams($stmt, $params);
            $stmt->execute();
        }

        return true;
    }

    /**
     * @param string $sql
     * @param array $params
     * @return array|null
     */
    public function fetchAll($sql, $params = [])
    {
        if ($stmt = $this->connection->prepare($sql)) {
            $stmt = $this->bindParams($stmt, $params);
            $stmt->execute();

            return $stmt->fetchAll();
        }

        return null;
    }

    /**
     * @param string $sql
     * @param array $params
     * @return bool|array
     */
    public function fetchRow($sql, $params = [])
    {
        $results = $this->fetchAll($sql, $params);

        return empty($results) ? false : array_shift($results);
    }

    /**
     * @return mixed
     */
    public function lastInsertId()
    {
        return $this->connection->lastInsertId();
    }

    /**
     * @param $stmt
     * @param $params
     * @return mixed
     */
    private function bindParams($stmt, $params)
    {
        if (!empty($params)) {
            foreach ($params as $index => $param) {
                $stmt->bindValue($index + 1, $param['value'], $param['type']);
            }
        }

        return $stmt;
    }

}