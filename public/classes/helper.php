<?php

class Helper
{

    public function getAccountByUsername($username)
    {
        return App()->Db()->fetchRow('SELECT * FROM `accounts` WHERE `username`=?;', [
            ['value' => $username, 'type' => \PDO::PARAM_STR]
        ]);
    }

}