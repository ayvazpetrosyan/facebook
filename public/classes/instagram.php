<?php

class Instagram
{

    public function getAccessToken($code, $test = false)
    {
        $redirectUrl = PROJECT_CONFIG['domain'] . 'auth';
        $url = 'https://api.instagram.com/oauth/access_token';

        $data = [
            'client_id' => PROJECT_CONFIG['clientId'],
            'client_secret' => PROJECT_CONFIG['clientSecret'],
            'grant_type' => 'authorization_code',
            'redirect_uri' => $redirectUrl,
            'code' => $code
        ];

        if ($test) {
//            $data['client_id'] = '449135174457872';
//            $data['client_secret'] = 'b166c11486b9f18d819b54ddabeb7f6d';
            $data['client_id'] = '762304692347894';
            $data['client_secret'] = '1c015a18362f5b0076c63c305eb1b968';
        }

        $results = $this->curl($url, $data);
        $results = json_decode($results, true);
        if (isset($results['access_token']) && isset($results['user_id'])) {
            $accessToken = $results['access_token'];
        } else if (isset($results['error_message'])) {
            return ['success' => false, 'message' => $results['error_message']];
        } else {
            return ['success' => false, 'message' => 'Unidentified error'];
        }
        $userId = $results['user_id'];
        $expires = time() + 3600;
        $results = $this->exchangeForLongLivedAccessToken($accessToken, $data['client_secret']);
        if (empty($results['error']) && !empty($results['access_token']) && !empty($results['expires_in'])) {
            $accessToken = $results['access_token'];
            $expires = time() + $results['expires_in'];
        }

        return ['success' => true, 'token' => $accessToken, 'user_id' => $userId, 'expires' => $expires];
    }

    /**
     * @param string $userId
     * @param string $accessToken
     * @param array $fields
     * @return array
     */
    public function getMedia($userId, $accessToken, $fields = ['caption', 'id', 'media_type', 'media_url', 'permalink', 'thumbnail_url', 'timestamp', 'username'])
    {
        $url = 'https://graph.instagram.com/' . $userId . '/media?' . (!empty($fields) ? ('fields=' . implode(',', $fields) . '&') : '') . 'access_token=' . $accessToken;
        $results = $this->curl($url);
        $results = json_decode($results, true);
        if (isset($results['error'])) {
            return ['success' => false, 'message' => $results['error']['message']];
        }

        return ['success' => true, 'media' => $results];
    }

    /**
     * @param string $userId
     * @param string $accessToken
     * @param array $fields
     * @return array
     */
    public function getUser($userId, $accessToken, $fields = ['id', 'username'])
    {
        $url = 'https://graph.instagram.com/' . $userId . '?' . (!empty($fields) ? ('fields=' . implode(',', $fields) . '&') : '') . 'access_token=' . $accessToken;
        $results = $this->curl($url);
        $results = json_decode($results, true);
        if (isset($results['error'])) {
            return ['success' => false, 'message' => $results['error']['message']];
        }

        return ['success' => true, 'user' => $results];
    }

    /**
     * @param $token
     * @param $clientSecret
     * @return mixed
     */
    public function exchangeForLongLivedAccessToken($token, $clientSecret)
    {
        $url = 'https://graph.instagram.com/access_token?grant_type=ig_exchange_token&client_secret=' . $clientSecret . '&access_token=' . $token;
        $results = $this->curl($url);

        return json_decode($results, true);
    }

    /**
     * @param $url
     * @param array $data
     * @return bool|string
     */
    private function curl($url, $data = [])
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CUSTOMREQUEST => empty($data) ? "GET" : "POST"
        ]);

        if (!empty($data)) {
            $postFields = "";
            foreach ($data as $key => $value) {
                $postFields .= $key . "=" . $value . "&";
            }
            $postFields = substr($postFields, 0, -1);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postFields);
        }

        $results = curl_exec($curl);

        return $results;
    }

}
