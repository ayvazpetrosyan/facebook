<?php

class Application
{

    private static $instance = null;

    /**
     * @var Db
     */
    private $db;

    /**
     * @var Instagram
     */
    private $instagram;

    /**
     * @var Helper
     */
    private $helper;

    /**
     * Application constructor.
     */
    private function __construct()
    {
        $this->db = new Db();
        $this->instagram = new Instagram();
        $this->helper = new Helper();
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new Application();
        }

        return self::$instance;
    }

    /**
     * @return Db
     */
    public function Db()
    {
        if (empty($this->db)) {
            $this->db = new Db();
        }

        return $this->db;
    }

    /**
     * @return Instagram
     */
    public function Instagram()
    {
        if (empty($this->instagram)) {
            $this->instagram = new Instagram();
        }

        return $this->instagram;
    }

    /**
     * @return Helper
     */
    public function Helper()
    {
        if (empty($this->helper)) {
            $this->helper = new Helper();
        }

        return $this->helper;
    }
}

function App()
{
    return Application::getInstance();
}