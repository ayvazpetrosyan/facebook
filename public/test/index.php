<?php

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

include("defines.php");
require_once __DIR__ . '/vendor/autoload.php';

$creeds = [
    'app_id' => APP_ID,
    'app_secret' => APP_SECRET,
    'default_graph_version' => GRAPH_API_VERSION,
    'persistent_data_handler' => 'session'
];

$homeUrl = getHomeUrl();

try {
    $facebook = new Facebook($creeds);
} catch (FacebookSDKException $e) {
    echo $e->getMessage();
    exit();
}

$helper = $facebook->getRedirectLoginHelper();

$oAuth2Client = $facebook->getOAuth2Client();

$loginUrl = null;
$instagramLoginUrl = '';
$accessToken = null;
$firstAccountAccessToken = null;
$imagesData = [];
$endpoints = [];

if (isset($_GET['code'])) {
    try {
        $accessToken = $helper->getAccessToken();

        $facebookPages = json_decode(curl("https://graph.facebook.com/" . $creeds['default_graph_version'] . "/me/accounts?access_token=$accessToken"));
        $pagesData = $facebookPages->data;
        $firstAccount = $pagesData[0];
        $firstAccountAccessToken = $firstAccount->access_token;

        $imageResult = json_decode(curl("https://graph.facebook.com/me/photos?fields=id,created_time,link&access_token=$firstAccountAccessToken"));
        $imagesData = $imageResult->data;

        $endpoints = [[
            'link' => "https://graph.facebook.com/me/tags?fields=id,username&access_token=$firstAccountAccessToken",
            'html' => "<a href='https://graph.facebook.com/me/tags?fields=id,username&access_token=$firstAccountAccessToken'>Get media object</a>"
        ], [
            'link' => "https://graph.facebook.com/me?metadata=1&access_token=$firstAccountAccessToken",
            'html' => "<a href='https://graph.facebook.com/me?metadata=1&access_token=$firstAccountAccessToken'>Get metadata</a>"
        ], [
            'link' => "https://graph.facebook.com/me/photos?fields=id,created_time,link&access_token=$firstAccountAccessToken",
            'html' => "<a href='https://graph.facebook.com/me/photos?fields=id,created_time,link&access_token=$firstAccountAccessToken'>Get photos</a>"
        ]];
    } catch (FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit();
    } catch (FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit();
    }
} else {
    $permissions = ['public_profile', 'instagram_basic', 'pages_show_list', 'instagram_manage_comments', 'pages_read_engagement'];
    $loginUrl = $helper->getLoginUrl(FACEBOOK_REDIRECT_URI, $permissions);
    $instagramLoginUrl = 'https://api.instagram.com/oauth/authorize?client_id=' . $creeds['app_id'] . '&redirect_uri=' . FACEBOOK_REDIRECT_URI . '&scope=user_profile,user_media&response_type=code';
}

/**
 * @return string
 */
function getHomeUrl(): string
{
    if (isset($_SERVER['HTTPS'])) {
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    } else {
        $protocol = 'http';
    }
    return $protocol . "://" . $_SERVER['HTTP_HOST'] . '/test';
}

/**
 * @param $url
 * @param array $data
 * @return bool|string
 */
function curl($url, array $data = [])
{
    $curl = curl_init();

    curl_setopt_array($curl, [
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_CUSTOMREQUEST => empty($data) ? "GET" : "POST"
    ]);

    if (!empty($data)) {
        $postFields = "";
        foreach ($data as $key => $value) {
            $postFields .= $key . "=" . $value . "&";
        }
        $postFields = substr($postFields, 0, -1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postFields);
    }

    return curl_exec($curl);
}

/**
 * @param $parameters
 * @param bool $exit
 * @return void
 */
function customDebug($parameters, bool $exit = true)
{
    echo '<pre>';
    print_r($parameters);
    echo '</pre>';
    if ($exit) exit();
}

?>

<html lang="">
<head>
    <title></title></head>
<body>
<p><a href="<?= $homeUrl ?>">Home</a></p>
<?php if (empty($loginUrl)) { ?>
    <p>
        <b>Access token:</b>
        <br>
        <?= $accessToken ?>
    </p>
    <p>
        <b>First account access token:</b>
        <br>
        <?= $firstAccountAccessToken ?>
    </p>
    <div class="images-block block">
        <?php foreach ($imagesData as $imageInfo) { ?>
            <div class="image-block">
                <img alt="" src="<?= $imageInfo->link ?>"
            </div>
        <?php } ?>
    </div>
    <div class="endpoints-block block">
        <?php foreach ($endpoints as $endpoint) { ?>
            <p>
                <?= $endpoint['link'] ?>
                <br>
                <?= $endpoint['html'] ?>
            </p>
        <?php } ?>
    </div>
<?php } else { ?>
    <div class="facebook-login-block">
        <p><a href="<?= $loginUrl ?>">Facebook login</a></p>
        <p><a href="<?= $instagramLoginUrl ?>">Instagram login</a></p>
    </div>
<?php } ?>
</body>
</html>
