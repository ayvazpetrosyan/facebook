<?php

namespace classes;

use Throwable;

class Download
{
    const IMAGES_DIR_PATH = __DIR__ . '../images';

    public function __construct()
    {
    }

    public function downloadImages()
    {
        $this->createImagesDir();

    }

    /**
     * @return void
     */
    private function createImagesDir()
    {
        try {
            if (is_dir(self::IMAGES_DIR_PATH)) {
                rmdir(self::IMAGES_DIR_PATH);
            }
            mkdir(self::IMAGES_DIR_PATH);
        } catch (Throwable $e) {
            exit($e->getMessage());
        }
    }
}
