<?php

$actions = [
    'index', 'auth', 'download', 'deauth', 'delete', 'validate'
];
$action = !empty($_SERVER['REDIRECT_URL']) ? strtolower(trim($_SERVER['REDIRECT_URL'], '/')) : 'index';
if (!in_array($action, $actions) || !file_exists(__DIR__ . '/actions/' . $action . '.php')) {
    echo 'Action not available';
    exit;
}
session_start();

include __DIR__ . '/autoload.php';

$className = ucfirst($action);
$actionClass = new $className();

$actionClass->run();
